==================================
Welcome to Varwin's documentation!
==================================

Varwin is a virtual reality platform that streamlines content development in VR
Perfect for VR studios, enterprise teams and content developers

.. toctree::
   :caption: Varwin platform
   :maxdepth: 1
   
   Varwin platform: general information <platform/general>
   Equipment and system requirements <platform/equipment>
   Varwin RMS App <platform/rms>
   Roles and functions of Varwin users <platform/roles>
   SDK <sdk/sdk>