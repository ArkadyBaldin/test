=====================================================
Equipment and system requirements for Varwin platform
=====================================================

The Varwin platform requires: 

- VR equipment set
- personal computer fitting the following system requirements.

VR equipment
============

1. VR equipment set (options a-c at choice):

 **a. HTC Vive** 
 
 - full kit: VR headset - 1 pcs, wireless controllers - 2 pcs, base stations - 2 pcs., accessories
 - photo stands - might be useful for base stations placement, depending on room characteristics and desired arrangement of the base stations.
	  
 **b. Oculus** 
 
 - VR headset - 1 pcs, wireless controllers - 2 pcs, Oculus sensors - 2 pcs, accessories
 
 **c. Windows Mixed Reality (WMR) (any manufacturer)**
 
 - VR headset - 1 pcs, wireless controllers - 2 pcs, accessories
 - WMR kit doesn’t require any additional movement trackers
 
Please note: the Desktop Editor and Desktop Player features allow to create VR projects on the Varwin platform even without VR equipment.  
Learn more: Editing without VR (Desktop editor), Preview without VR (Desktop Player)

PC system requirements
======================

1. OS: Windows 10 and higher, x64    
2. Processor: Intel Core i5 - i7* or AMD Ryzen 5
3. Memory: 8 Gb and higher
4. Graphics card: GeForce GTX 1060 6 Gb video memory and higher* (or similar in performance)  
5. 10+ Gb hard drive free space
6. USB 3.0 motherboard and power supply unit, corresponding with the above specifications
7. **(for WMR)**: Bluetooth 4.0
8. Video output: HDMI 1.4, 2.0 or DisplayPort 1.3 (depending on the chosen equipment requirements)

\*\ depending on content complexity

Compatibility check
===================

VR equipment manufacturers offer compatibility check tools for Windows-based PCs. Choose equipment of interest and download compatibility check tool from official sources.

HTC Vive compatibility check http://dl4.htc.com/vive/ViveCheck/ViveCheck.exe
Windows Mixed Reality compatibility check https://www.microsoft.com/en-us/store/p/windows-mixed-reality-pc-check/9nzvl19n7cnc?rtc=1
Oculus Rift compatibility check https://ocul.us/compat-tool

Software
========

The following software must be installed  on your PC in order for the Varwin system to operate:

1. Steam
2. Steam VR
3. **(for Windows Mixed Reality equipment)**: Windows Mixed Reality for SteamVR
4. Unity 2018.4.1f1 (for creating objects and/or scene templates)

For detailed instructions on required software installation, please see Varwin installation and launch - Preparing your PC. https://docs.google.com/document/d/154fkmOUdvtJHYM0NmCNBLEfukRp2kzetg4Zc6PGXNBE/edit#heading=h.vnz3cjxmkw24
