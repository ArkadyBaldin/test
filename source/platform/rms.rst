==============
Varwin RMS App
==============

Varwin app is the environment for creating and editing VR projects. 
Varwin app is installed on computers of everyone participating in a project.

App modes
=========

Edit mode
---------

Editing in VR
^^^^^^^^^^^^^

.. image:: media/image21.png

- Edit mode in VR is used to spawn objects on the scene
- In Edit Mode, physical laws do not affect objects. They are applied in Preview and View modes.

Editing without VR: Desktop Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use your mouse and keyboard to edit VR scenes. The Desktop Editor feature allows editing without VR, namely place objects on the scene and set their positions. To use this feature, click “Edit on desktop.”

.. image:: media/image7.png

*Learn more:* Adding objects without VR (Desktop editor) https://docs.google.com/document/d/1ji8IlKHhGR5aJDMzmPGu1I-DVEx0i7xq8PQAft2oDqo/edit#heading=h.ek19qlqc8jx6

Creating the project scenario
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Project scenario and logic are created in Blockly visual editor. No programming skills are required. 

.. image:: media/image4.png

.. image:: media/image16.png

*Working with Blockly:* Manual https://drive.google.com/open?id=1J6C6y2byfLWf3ojLXNg2hGarLVvGczTvG4e_aG42Hi4

- All necessary logic blocks are provided 
- Users can create and name necessary variables

.. image:: media/image24.png

*Pre-provided logic blocks*

.. image:: media/image18.png

*Variables creation option; custom variables*

Code editing
^^^^^^^^^^^^

If necessary, the user can edit the code of the project.

.. image:: media/image12.png

Preview mode
------------

- In Preview mode, you can evaluate each scene of your future project separately.
- The Preview mode works both in VR or in Desktop Editor. 
- Physical laws are applied to objects*. 
- The objects engage in the preset logic (e.g., a display shows specific text when specific conditions are met.) 

\*\ Those objects which have been assigned relevant parameter when created

Preview in VR
^^^^^^^^^^^^^

.. image:: media/image19.png

Preview on desktop (Desktop Player)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: media/image23.png

*Learn more:* Preview without VR (Desktop Player) https://docs.google.com/document/d/1ji8IlKHhGR5aJDMzmPGu1I-DVEx0i7xq8PQAft2oDqo/edit#heading=h.pwli4eg7n1wz

View mode
---------

View mode allows the user to evaluate the whole project before building it.

.. image:: media/image2.png

View mode can be switched on if the project has at least one configuration.

.. image:: media/image22.png

*Learn more:* App launch configurations and view https://docs.google.com/document/d/1ji8IlKHhGR5aJDMzmPGu1I-DVEx0i7xq8PQAft2oDqo/edit#heading=h.32bbtxuwhe4q

- Physical laws are applied to all objects.
- View mode doesn’t allow to switch to Edit mode, nor to make any editing. 
- Interaction with configuration (ready-made virtual world) following a preset script.

*See also:* VR menu functions https://docs.google.com/document/d/1ji8IlKHhGR5aJDMzmPGu1I-DVEx0i7xq8PQAft2oDqo/edit#heading=h.tyjcwt

Library
=======
Varwin app provides the user with a library storing the following:

- project settings,
- project metadata,
- scene templates to create scenarios in,
- objects to set in the scene templates.

Varwin allows users to create new objects and scene templates and upload them to the library. This will require Varwin SDK - development tools tailored for Varwin platform and provided in the packaged solution.
*Learn more:* Varwin SDK https://docs.google.com/document/d/1p0nfwNZRvvWYq2aG9HXmGROw-v8HBkZ5X2kDeyAKwp4

Project structures
------------------

.. image:: media/image13.png

*Projects metadata in the library*

Project metadata is a blank VR project for further editing. It contains IDs of scene templates and objects, coordinates of objects’ placement, the logic for Blockly. This file is editable.

Scene templates and objects
---------------------------

Scene templates and objects are stored in the relevant sections of the Library. Both their names and icons are displayed in the list. 

.. image:: media/image11.png

*Scene templates in the library*

.. image:: media/image8.png

*Objects in the library*

.. image:: media/image20.png

*Object menu in VR*

Versioning of scene templates and objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An object/scene template can have different versions. An RMS user can view versions of an object/scene template by clicking the Default Version button in the RMS.

.. image:: media/image14.png

- The latest version of an object/scene template is set by the system as a default one. 

.. image:: media/image10.png

- If a user selects another version of an object/scene template as a default one, a sign of a lock appears on the Default Version button. It means that a user default version is selected for this object/scene template. 

.. image:: media/image15.png

.. image:: media/image3.png

- This choice is fixed for all the projects. 
- If an RMS user uses a version of an object/scene template in a project and then changes the default version for this object/scene template in the library, the change won’t affect that project.
- There can be any number of versions to choose from.

Tags
----

Tags can be assigned to objects and/or scene templates in order to simplify their finding and systematizing.

.. image:: media/image9.png

*Tags in the library*

- The tags assigned in the library will be displayed in VR. 

.. image:: media/image25.png

*Tags in VR*

- Tags are editable
- For tag list in VR, click Filter. The digit icon on this button displays the number of tags applied. 

.. image:: media/image6.png

*Tag list in VR*

Files for export/import 
-----------------------

Files containing objects, scene templates, project metadata can be imported into the system. 
Project metadata and ready apps can be exported as files. Scene templates and objects are not exportable.

.. image:: media/image17.png

*Import of objects, scene templates, project metadata*

.. image:: media/image1.png

*Export of project metadata*

End product
===========

When the project is completed, an app for the end user can be built out of it. 
- The apps are exported as .exe files. They can be launched on any PC. 
- Varwin platform installation is not needed to launch an exported app. 
- The apps are not editable.
- Not available for Starter edition. 

.. image:: media/image5.png

*Build a ready app (not available for Starter edition)*

