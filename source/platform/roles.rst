===================================
Roles and functions of Varwin users
===================================

Administrator
=============

*(currently, this function can be carried out by any user)*

- Installs and launches Varwin platform

Instructions and related documents
----------------------------------

Varwin installation and launch https://drive.google.com/open?id=154fkmOUdvtJHYM0NmCNBLEfukRp2kzetg4Zc6PGXNBE
Equipment and system requirements for Varwin platform https://docs.google.com/document/d/1YnVCUts-NDuexkL_8lpMPlc_u6LR5m7oym-3rylly-g
Roles and functions of Varwin users https://docs.google.com/document/d/1ZPUuc5oxc4WhBYsqIWoD070IXqvT9DzpO5ZEOGKF9Zk
FAQ https://drive.google.com/open?id=1C0YLWehIRsEa1_FQLs9cR_uQ9UReMQ2B8Wcd7WJY3f8

Creator of objects & scene templates 
====================================

*(most likely, a Unity programmer)*

- Creates objects and scene templates out of ready-made prefabs in Unity
- Writes logic of their interaction
- Makes Blockly blocks for the objects

Instructions and related documents
----------------------------------

Varwin SDK https://drive.google.com/open?id=1p0nfwNZRvvWYq2aG9HXmGROw-v8HBkZ5X2kDeyAKwp4
Creating objects for Varwin in Unity https://docs.google.com/document/d/1hsiaY3RPNxpEiCmi49X1WuThOj3C-a0yksD43CJXfFc
Creating scene templates for Varwin in Unity https://docs.google.com/document/d/1mvkpk0t4nBP0NpLNd2PJF2Qnd50U_8rKKRfS_74dEEs
Uploading content into Varwin library from Unity https://docs.google.com/document/d/1CR4Gv82DdOQaPDJdPPhMyUL5ulUO7vOX2BLG0XSsWcc
Creating libraries with shared code https://docs.google.com/document/d/1CR4Gv82DdOQaPDJdPPhMyUL5ulUO7vOX2BLG0XSsWcc
Public interfaces https://docs.google.com/document/d/1IW4qTlWNJSrS06jKD5ASJxVkK0wSmA_xFskRUNj1BBM
FAQ https://drive.google.com/open?id=1nFkmWVYxjEHAd8lg3hw0oOnx_vBmHMJBziBYwffnZ0U

Project creator
===============

- Uploads new objects and scene templates to the library
- Creates and edits projects
- Creates project scenario using objects and scene templates from the library and blocks in Blockly
- Shapes up requirements for the new objects and Blockly blocks, or selects from ready ones.

Instructions and related documents
----------------------------------

Varwin platform: general information https://docs.google.com/document/d/1T840LvW5hMn4BTiyWYsUDwVDCJV_7j4sRIHqoZ7btqQ
Varwin RMS App https://docs.google.com/document/d/1LOg1Ts-gDaG43nlDBW80sas961XDlgt1dFUqQe9Ntx8
Using VR controllers https://docs.google.com/document/d/103KShWrxmJBIRwkcPeKaJNXoTDSB_9Ov_1iI8LjbtI4
Creating VR projects https://docs.google.com/document/d/1ji8IlKHhGR5aJDMzmPGu1I-DVEx0i7xq8PQAft2oDqo
Working with Blockly https://docs.google.com/document/d/1J6C6y2byfLWf3ojLXNg2hGarLVvGczTvG4e_aG42Hi4
FAQ https://drive.google.com/open?id=1C0YLWehIRsEa1_FQLs9cR_uQ9UReMQ2B8Wcd7WJY3f8

Varwin RMS tutorial

.. image:: media/image26.png

End-user
========

- Launches end product (an app built out of VR project) from Varwin interface or from .exe file
- Interacts with the end product

Instructions and related documents
----------------------------------

Equipment and system requirements for Varwin platform https://docs.google.com/document/d/1YnVCUts-NDuexkL_8lpMPlc_u6LR5m7oym-3rylly-g
Using VR controllers https://docs.google.com/document/d/103KShWrxmJBIRwkcPeKaJNXoTDSB_9Ov_1iI8LjbtI4
Varwin RMS App https://docs.google.com/document/d/1LOg1Ts-gDaG43nlDBW80sas961XDlgt1dFUqQe9Ntx8
FAQ https://drive.google.com/open?id=1C0YLWehIRsEa1_FQLs9cR_uQ9UReMQ2B8Wcd7WJY3f8


